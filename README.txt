About:
The Commerce Shipping Quick Estimate Module was built to provide means for
retrieving shipping estimates without having to go through the entire 
checkout process. The module currently implements an additional block, 
"Shipping Estimate Calculator", which must be enabled and placed within 
a region on your site.

Requirements:
-Drupal Commerce (https://www.drupal.org/project/commerce)
-Drupal Commerce Cart (https://www.drupal.org/project/commerce)
-Drupal Commerce Shipping (https://www.drupal.org/project/commerce_shipping)

Instructions:
1-Download the module below
2-Upload it to /sites/all/modules folder
3-Enable the module via the Modules page (/admin/modules)
4-Enable and place the block as necessary via the Blocks page 
(/admin/structure/block)

Current Limitations
-This module is currently set to pull estimates from within the United States. 
International support has not been implemented
-No fallback for when JavaScript is disabled

Common Issues
-No shipping methods returned - 
Make sure you have enabled and configured a shipping method. 
This module relies on the Commerce Shipping Module 
(https://www.drupal.org/project/commerce_shipping)
